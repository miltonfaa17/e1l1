
package prlec;

import java.io.*;

public class textolec {
    public String lecturaTXT(String ubicacion){
        String text = "";
        try{
            BufferedReader bf = new BufferedReader(new FileReader(ubicacion));
            String vt = "";
            String bfleec;
            while((bfleec = bf.readLine()) != null){
            vt = vt + bfleec;
        }
            text = vt;
        }catch(Exception e){
            System.err.println("archivo no encontrado");
        }
        return text;
    }
}
